<?php
require("phpFiles/admin/databaseConnect.php");

session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SebenzaGroup calculator</title>
    <script src="js/jquery-1.12.3.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/main-css/main.css">

    <!-- Latest compiled and minified JavaScript -->

    <script src="js/xepOnline.jqPlugin.js"></script>
    <script src="js/datatables/datatables.min.js"></script>
    <script src="js/jq-vertical-dot-nav.js"></script>
    <script src="js/jquery.sticky-kit.js"></script>
    <script src="js/bootstrap.js"></script>
</head>
<body style="padding-top:70px">
<nav style="width: 100%" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Sebenza</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php

                if (isset($_SESSION["role"])) {
                    echo "<li><a href=\"admin.php\">Admin<span class=\"sr-only\">(current)</span></a></li>";
                }

                ?>
                <li class="active"><a href="calculators.php">Calculator</a></li>
                <li><a target="_blank" href="http://sebenzacc.spiceware.net/">File-O-Mint</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="calcBackground">
    <div class="calculator-center" style="padding-top: 10px;">
        <div class="container" style="width: 100%">
            <div class="row">
                <div class="col-md-6">
                    <div class="container" style="width: 100%">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div style="padding: 5px; border: solid 2px grey; border-radius: 3px; margin-top: 10px; margin-bottom: 10px">
                                    <h3 style="text-align: center; text-shadow: 1px 1px grey; opacity: 0.7">Select
                                        Department and Services</h3>
                                </div>
                                <div style="padding: 5px; border: solid 2px grey; border-radius: 3px; opacity: 0.8">
                                    <div class="topButtons">
                                        <?php
                                        $sql = "SELECT * FROM  columnList ORDER BY columnName";

                                        $result = $conn->query($sql);
                                        $count = 0;
                                        //if there are rows then execute the code below
                                        if ($result->num_rows > 0) {
// output data of each row
                                            while ($row = $result->fetch_assoc()) {
                                                $name[$count] = $row["columnName"];
                                                $count++;
                                            }
                                        }

                                        for ($num = 0; $num < $count; $num++) {

                                            $finalName = str_replace(' ', '-', $name[$num]);

                                            echo "   <a class=\"btn btn-primary\" role=\"button\" data-toggle=\"collapse\"
                                           href=\"#$finalName\"
                                           aria-expanded=\"false\" aria-controls=\"collapseExample\">
                                            $name[$num]
                                        </a>";
                                        }

                                        ?>

                                    </div>

                                    <?php

                                    //if there are rows then execute the code below

                                    for ($j = 0; $j < $count; $j++) {
                                        $sql = "SELECT * FROM  rowList WHERE columnName = '$name[$j]' ORDER BY columnOrderNum ASC";

                                        $result = $conn->query($sql);

                                        $columnName = str_replace(' ', '-', $name[$j]);

                                        echo "
                                        <div class='collapse' id=\"$columnName\">
                                        <div class=\"well\">
                                            <h2>
                                                $name[$j]
                                            </h2>
                                            <div class=\"btn-group-vertical\" role=\"group\" aria-label=\"...\">
                                        ";
                                        if ($result->num_rows > 0) {
// output data of each row
                                            while ($row = $result->fetch_assoc()) {
                                                $rowName = $row["rowName"];
                                                $rowCode = $row["rowCode"];
                                                echo "
                                                <button class=\"btn btn-default\" type=\"button\" data-toggle=\"modal\"
                                                        data-target=\"#$rowCode\">$rowName
                                                </button>
                                               
                                          ";
                                            }
                                        }
                                        echo " </div>
                                        </div>
                                    </div>";
                                    }

                                    ?>

                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="container" style="width: 100%">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="result">
                                    <div id="selectShow" style="display: none;" ;>
                                        <div style="padding: 5px; border: solid 2px grey; border-radius: 3px; margin-top: 10px; margin-bottom: 10px">
                                            <h3 style="text-align: center; text-shadow: 1px 1px grey; opacity: 0.7">
                                                Selected Services</h3>
                                        </div>
                                        <div style="padding: 5px; border: solid 2px grey; border-radius: 3px; margin-top: 10px; margin-bottom: 10px">
                                            <div class="panel-group" id="accordion" role="tablist"
                                                 aria-multiselectable="true">

                                                <?php

                                                for ($k = 0; $k < $count; $k++) {
                                                    $correctColumn = str_replace(' ', '-', $name[$k]);
                                                    echo "<div class='$correctColumn-Result panel panel-default' style='display: none'>
                                            <div class=\"panel-heading\" role=\"tab\" id='heading$k'>
                                                <h4 class=\"panel-title\"><a role=\"button\" data-toggle=\"collapse\"
                                                               data-parent=\"#accordion\"
                                                               href=\"#collapse$k\"
                                                               aria-expanded=\"true\" aria-controls=\"collapse$k\">$name[$k]
                                                    <div class=\"pull-right\"
                                                         style=\"font-size: 12px; padding-right: 10px\">
                                                        Monthly retainer total: R<span 
                                                                id=\"$correctColumn-TotalMonth\">0</span> | Once-off total: R<span
                                                                id=\"$correctColumn-Total\">0</span></div>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id='collapse$k' class=\"panel-collapse collapse in\" role=\"tabpanel\"
                                                 aria-labelledby=\"heading$k\">
                                                <div class=\"panel-body $name[$k]Append\">
                                                <span class='$name[$k]-Sort'>
                                                <h5>
                                                Options:
                                                <span onclick='adminFees(\"$correctColumn\")' class='sortBtn glyphicon glyphicon-usd pull-right'></span>
                                                <span onclick='generateModulePDF(\"$correctColumn-class\",\"$correctColumn-id\"),hidediv();' class='sortBtn glyphicon glyphicon-print pull-right' ></span>
                                                <input value='0' hidden id='$correctColumn-FEE'>
                                                </h5>

                                                    <hr>
                                                </span>
";
                                                    $colName = $name[$k];
                                                    $sql = "SELECT * FROM rowList WHERE columnName = '$colName' ORDER BY columnOrderNum ASC";

                                                    $result = $conn->query($sql);

                                                    if ($result->num_rows > 0) {

                                                        echo "<span class='$correctColumn-adminFee'></span>";

                                                        while ($row = $result->fetch_assoc()) {
                                                            $rowCode = $row["rowCode"];
                                                            $colName = $row["columnName"];
                                                            echo "<span class='$rowCode-id $name[$k]-Col'></span>";
                                                        }

                                                    }

                                                    echo "
   </div>
                                            </div>
                                        </div>";
                                                }
                                                ?>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="result" id="results" style="display: none">
                                        <h2>Sebenza Accounting PTY LTD</h2>
                                        <br>
                                        <h2>Quotation:<span id="clientName"></span></h2>
                                        <div id="monthlyResults" style="display: none">
                                            <h4 id="monthlyText"
                                                style="border: 1px solid darkgoldenrod; background-color: lightseagreen; color: white;padding-top: 4px; padding-bottom: 4px; padding-left: 4px;bottom: 4px">
                                                Included in Retainer</h4>
                                            <div class="panel-group" id="accordionM" role="tablist"
                                                 aria-multiselectable="true">

                                                <?php

                                                for ($k = 0; $k < $count; $k++) {
                                                    $correctColumn = str_replace(' ', '-', $name[$k]);
                                                    echo "<div class='$correctColumn-ResultM $correctColumn-class' id='$correctColumn-idM' style='display: none'>
                                            <div class=\"panel-heading\" role=\"tab\">
                                                <h4><p>$name[$k]
                                                    <div class=\"pull-right\"
                                                         style=\"font-size: 12px; padding-right: 10px\">
                                                        Monthly Retainer: R<span
                                                                id=\"$correctColumn-TotalM-M\"></span> | Year Total: R<span attr='TotalM'
                                                                id=\"$correctColumn-TotalM\">0</span></div>
                                                    </p>
                                                </h4>
                                            </div>
                                            <div class=\"panel-collapse collapse in\" role=\"tabpanel\"
                                                 aria-labelledby=\"headingOne\">
                                                <div class=\"panel-body\">
";

                                                    $sql = "SELECT * FROM rowList WHERE payRetain = '1' AND columnName = '$name[$k]' ORDER BY columnOrderNum ASC";

                                                    $result = $conn->query($sql);

                                                    if ($result->num_rows > 0) {

                                                        echo "<span class='$correctColumn-adminFee'></span>";
                                                        while ($row = $result->fetch_assoc()) {
                                                            $rowCode = $row["rowCode"];
                                                            echo "<span class='$rowCode-idM'></span>";
                                                        }
                                                    }
                                                    echo "
                                              </div>
                                            </div>
                                        </div>";
                                                }
                                                ?>

                                            </div>
                                            <div id="footerM-vatIncl" style="display: none">
                                                <p style="border: 1px solid darkgoldenrod; background-color: lightseagreen; color: white; height: 120px; padding-top: 4px; padding-bottom: 4px; padding-left: 4px;padding-right: 4px; margin-bottom: 10px"
                                                   class="pull-right">
                                                    Year
                                                    Subtotal R<span id="YearlyM-Total"></span>
                                                    <br>
                                                    VAT R<span id="YearlyM-VAT"></span>
                                                    <br>
                                                    Total Incl R<span id="YearlyM-VATI"></span>
                                                    <br>
                                                    Retainer Total R<span id="monthly-Total"></span>
                                                    <br>
                                                </p>
                                            </div>
                                            <div id="footerM-vatExcl" style="display: none">
                                                <p style="border: 1px solid darkgoldenrod; background-color: lightseagreen; color: white; height: 50px; padding-top: 4px; padding-bottom: 4px; padding-left: 4px;padding-right: 4px; margin-bottom: 10px"
                                                   class="pull-right">
                                                    Year
                                                    Subtotal R<span id="YearlyM-Total-vatExcl"></span>
                                                    <br>
                                                    Retainer Total R<span id="monthly-Total-vatExcl"></span>
                                                    <br>
                                                </p>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div id="onceOffResults" style="display: none">
                                            <h4 id="onceOffText"
                                                style="border: 1px solid darkgoldenrod; background-color: lightseagreen; color: white; padding-top: 4px; padding-bottom: 4px; padding-left: 4px;bottom: 4px">
                                                Once off</h4>
                                            <div class="panel-group" id="accordionO" role="tablist"
                                                 aria-multiselectable="true">

                                                <?php

                                                for ($k = 0; $k < $count; $k++) {
                                                    $correctColumn = str_replace(' ', '-', $name[$k]);
                                                    echo "<div class='$correctColumn-ResultO $correctColumn-class' id='$correctColumn-idO' style='display: none'>
                                            <div class=\"panel-heading\" role=\"tab\">
                                                <h4><p>$name[$k]
                                                    <div class=\"pull-right\"
                                                         style=\"font-size: 12px; padding-right: 10px\">
                                                        Total: R<span
                                                                id=\"$correctColumn-TotalO\" attr='TotalO'>0</span></div>
                                                    </p>
                                                </h4>
                                            </div>
                                            <div class=\"panel-collapse collapse in\" role=\"tabpanel\"
                                                 aria-labelledby=\"headingOne\">
                                                <div class=\"panel-body\">
";

                                                    $sql = "SELECT * FROM rowList WHERE payOnce = '1' AND columnName = '$name[$k]' ORDER BY columnOrderNum ASC";

                                                    $result = $conn->query($sql);

                                                    if ($result->num_rows > 0) {

                                                        echo "<span class='$correctColumn-adminFee'></span>";

                                                        while ($row = $result->fetch_assoc()) {
                                                            $rowCode = $row["rowCode"];
                                                            echo "<span class='$rowCode-idO'></span>";
                                                        }
                                                    }


                                                    echo "
   </div>
                                            </div>
                                        </div>";
                                                }
                                                ?>

                                            </div>
                                            <div id="footerY-vatIncl" style="display: none">
                                                <p style="border: 1px solid darkgoldenrod; background-color: lightseagreen; color: white;height: 90px; padding-top: 4px; padding-left: 4px;padding-right: 4px"
                                                   class="pull-right">
                                                    Subtotal R<span id="YearlyY-Total"></span>
                                                    <br>
                                                    VAT R<span id="YearlyY-VAT"></span>
                                                    <br>
                                                    Total Incl R<span id="YearlyY-VATI"></span>
                                                </p>
                                            </div>
                                            <div id="footerY-vatExcl" style="display: none">
                                                <p style="border: 1px solid darkgoldenrod; background-color: lightseagreen; color: white;height: 25px; padding-top: 4px; padding-left: 4px;padding-right: 4px"
                                                   class="pull-right">
                                                    Subtotal R<span id="YearlyY-Total-vatExcl"></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div id="mainFooter">
                                            <p>
                                            <h4>Terms and Conditions</h4>
                                            <ul>
                                                <li>Please note that this Quotation is only valid for five (5)
                                                    business
                                                    days.
                                                </li>
                                                <li>To accept this quote please sign and return via e-mail to
                                                    info@sebenzagroup.co.za
                                                </li>
                                                <li>For monthly retainer amounts a debit order authorisation needs
                                                    to
                                                    accompany
                                                    this signed quote, please request one from
                                                    info@sebenzagroup.co.za
                                                </li>
                                                <li>For once off quotations and amounts we require a 50% deposit
                                                    before
                                                    any work
                                                    will commence with the balance becoming payable within five (5)
                                                    business
                                                    days
                                                    from completion of work requested.
                                                </li>
                                                <li>We reserve the right to cancel or review any quotation without
                                                    notice.
                                                </li>
                                                <li>This document only becomes a valid contractual agreement if
                                                    signed
                                                    by
                                                    both
                                                    parties.
                                                </li>
                                            </ul>
                                            <br>
                                            <h4>Banking details</h4>
                                            <ul>
                                                <li>Netbank</li>
                                                <li>Account holder: Sebenza Rekenmeesters</li>
                                                <li>Branch code: 166234</li>
                                                <li>Account number: 1662071833</li>

                                            </ul>
                                            <br>
                                            By Signing below you agree to the terms and conditions above and declare
                                            that
                                            you are fully authorised to bind the individual/company/trust to this
                                            obligation
                                            and that signature of this document will result in the issuing of the
                                            relevant
                                            invoices that will become due and payable depending on its nature.
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <div class="container" style="width: 100%">
                                                <div class="row">
                                                    <div class="col-md-2"></div>
                                                    <div class="col-md-3">
                                                        <div style="border: 0.5px solid grey"></div>
                                                        <br>
                                                        For
                                                        <br>
                                                        <span id="clientName3"></span>
                                                    </div>
                                                    <div class="col-md-2"></div>
                                                    <div class="col-md-3">
                                                        <div style="border: 0.5px solid grey"></div>
                                                        <br>
                                                        For
                                                        <br>
                                                        Sebenza Accounting
                                                    </div>
                                                    <div class="col-md-2"></div>
                                                </div>
                                            </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div id="sideMenu"
                                     style="position: fixed;margin-top: 10px; display: none; background-color: white; border-radius: 5px;padding: 5px; border: solid 2px grey;">
                                    <p>Enter client name and click generate for quotation
                                        <span id="Total" style="display: none"></span></p>
                                    <div class="input-group">
                                                            <span class="input-group-addon"
                                                            >Client Name</span>
                                        <input type="text"
                                               class="form-control" id="clientNames"
                                               aria-describedby="basic-addon3">
                                    </div>
                                    <br>
                                    <button type="button" class="btn btn-default" data-toggle="modal"
                                            data-target="#vatIncludeModal">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modals">

    <?php
    require("phpFiles/admin/genModal.php");
    for ($k = 0; $k < $count; $k++) {
        echo "<div class='modals-$name[$k]'>";
        genModal($name[$k]);
        echo "</div>";
    }
    ?>

    <div class="modal fade" id="vatIncludeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">VAT Included</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">Should the VAT be included or excluded?</div>
                        <div class="row">
                            <button type="button" class="btn btn-default" data-dismiss="modal"
                                    onclick="generatePDF($('#clientNames').val(),true), hidediv();">Included
                            </button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"
                                    onclick="generatePDF($('#clientNames').val(),false), hidediv();">Excluded
                            </button>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="adminFees" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><span id="adminFeeHeader"></span> admin fees</h4>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <input type="text" id="adminFeesInput" class="form-control" placeholder="Admin fees">
                        <span class="input-group-btn">
                            <button onclick="" id="adminFeeBtn" class="btn btn-primary" type="button">Enter</button>
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    <?php


    $sql = "SELECT * FROM Rates";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {

        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $internRate = $row["internRate"];
            $juniorBookRate = $row["juniorRateBook"];
            $juniorConsultRate = $row["juniorRateConsult"];
            $seniorBookRate = $row["seniorRateBook"];
            $seniorConsultRate = $row["seniorRateConsult"];
            $masterRate = $row["masterRate"];

            echo "
           var internRate = $internRate;
           var juniorRateBook = $juniorBookRate; 
           var juniorRateConsult = $juniorConsultRate;
           var seniorRateBook = $seniorBookRate;
           var seniorRateConsult = $seniorConsultRate;
           var masterRate = $masterRate;
            ";
        }
    }

    ?>


    var vatAmount = 15;

    function generateModulePDF(moduleClass, moduleId) {

        var arr = [moduleId + "O", moduleId + "M"];

        $("." + moduleClass)[0].style.display = 'block';
        $("." + moduleClass)[1].style.display = 'block';

        return xepOnline.Formatter.Format(arr, {render: 'newwin', filename: 'Dolf', pageMargin: '0.5in'});

    }

    function adminFees(columnName) {
        var percentage = $("#" + columnName + "-FEE").val();
        $("#adminFeeHeader").text(columnName.split('-').join(' '));
        $("#adminFeesInput").val(percentage);
        $("#adminFees").modal('show');

        $("#adminFeeBtn").attr("onclick", "doAdminFee('" + columnName + "')");
    }

    function doAdminFee(columnName) {
        $("#" + columnName + "-FEE").val($("#adminFeesInput").val());

        var data =
            "<div id='" + "H'> " +
            "<h4> Admin fee </h4>" +
            "<p> This fee is applied to all items under " + columnName.split('-').join(' ') + " </p>" +
            "<p>The admin fee is : " + $("#adminFeesInput").val() + "%</p>" +
            "<hr>";

        $("." + columnName + "-adminFee").append(data);

        $("#adminFees").modal('hide');

        Total(columnName,0,0,"");
    }

    function generatePDF(clientName, vatIncluded) {

        totPDFOnce(vatIncluded);
        totPDFMonth(vatIncluded);

        $('#onceOffResults').filter(function () {
            return $(this).find('div').length === $(this).find('div').filter(function () {
                return $(this).css('display') === 'none';
            }).length;
        }).hide();

        $('#monthlyResults').filter(function () {
            return $(this).find('div').length === $(this).find('div').filter(function () {
                return $(this).css('display') === 'none';
            }).length;
        }).hide();

        document.getElementById('clientName').innerHTML = clientName;
        document.getElementById('clientName3').innerHTML = clientName;


        document.getElementById('results').style.display = 'block';

        return xepOnline.Formatter.Format('results', {render: 'newwin', filename: 'Dolf', pageMargin: '0.5in'});

    }

    function hidediv() {
        document.getElementById('results').style.display = 'none';

        document.getElementById('footerY-vatExcl').style.display = 'none';
        document.getElementById('footerM-vatExcl').style.display = 'none';

        document.getElementById('footerM-vatIncl').style.display = 'none';
        document.getElementById('footerY-vatIncl').style.display = 'none';
    }

    function HoursFunc(ID, hours, retainer, internLvl, juniorBookLvl, juniorConsultLvl, seniorBookLvl, seniorConsultLvl, masterLvl, catagory, className, cat) {

        var ans;
        var total = 0;
        var monthTot = 0;

        if (internLvl == 'intern') {
            if (retainer == 'monthly') {
                +((total = hours * internRate).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = hours * internRate).toFixed(2))
            }

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: intern</p>" +
                "<p>The amount of hours : " + hours + "</p>";
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM'  class='" + catagory + "'>" + monthTot + "</></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>";
            }else{
                ans = ans + "<p class='" + catagory + "price'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>";
            }

        } else if (juniorBookLvl == 'juniorBook') {
            if (retainer == 'monthly') {
                +((total = hours * juniorRateBook).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = hours * juniorRateBook).toFixed(2))
            }

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Junior bookkeeper</p>" +
                "<p>The amount of hours : " + hours + "</p>";
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }else{
                ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }

        } else if (juniorConsultLvl == 'juniorConsult') {
            if (retainer == 'monthly') {
                +((total = hours * juniorRateConsult).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = hours * juniorRateConsult).toFixed(2))
            }

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Junior consultant</p>" +
                "<p>The amount of hours : " + hours + "</p>";
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }else{
                ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }

        } else if (seniorBookLvl == 'seniorBook') {
            if (retainer == 'monthly') {
                +((total = hours * seniorRateBook).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = hours * seniorRateBook).toFixed(2))
            }

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Senior bookkeeper</p>" +
                "<p>The amount of hours : " + hours + "</p>";
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }else{
                ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }

        } else if (seniorConsultLvl == 'seniorConsult') {
            if (retainer == 'monthly') {
                +((total = hours * seniorRateConsult).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = hours * seniorRateConsult).toFixed(2))
            }

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Senior Consultant</p>" +
                "<p>The amount of hours : " + hours + "</p>";
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }else{
                ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }

        } else if (masterLvl == 'master') {
            if (retainer == 'monthly') {
                +((total = hours * masterRate).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = hours * masterRate).toFixed(2))
            }
            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Master</p>" +
                "<p>The amount of hours : " + hours + "</p>";
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }else{
                ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }



        } else {
            +((total = hours * juniorRateBook).toFixed(2))

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Junior</p>" +
                "<p>The amount of hours : " + hours + "</p>";

            ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                "<br>" +
                "</div>" +
                "<hr>";
        }

        $("#sideMenu").show();
        $("#selectShow").show();

        $("." + catagory + "").show()

        var IDs = ID + "-id";
        var IDm = ID + "-idM";
        var IDo = ID + "-idO";

        display(retainer, IDs, IDm, IDo, cat, ans);
        Total(cat, total, monthTot, retainer);
    }

    function DynFixedFunc(ID, QTY, retainer, internLvl, juniorBookLvl, juniorConsultLvl, seniorBookLvl, seniorConsultLvl, masterLvl, catagory, className, cat) {

        var ans;
        var total = 0;
        var monthTot = 0;

        if (internLvl == 'intern') {
            if (retainer == 'monthly') {
                +((total = QTY * internRate).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = QTY * internRate).toFixed(2))
            }

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: intern</p>";
            if (retainer == 'monthly') {
                ans = ans + "<span class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM'  class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>";
            }else {
                ans = ans + "<p class='" + catagory + "price'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>";
            }


        } else if (juniorBookLvl == 'juniorBook') {
            if (retainer == 'monthly') {
                +((total = QTY * juniorRateBook).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = QTY * juniorRateBook).toFixed(2))
            }

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Junior bookkeeper</p>";
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>";
            }else{
                ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }

        } else if (juniorConsultLvl == 'juniorConsult') {
            if (retainer == 'monthly') {
                +((total = QTY * juniorRateConsult).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = QTY * juniorRateConsult).toFixed(2))
            }

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Junior consultant</p>";
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }else{
                ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }

        } else if (seniorBookLvl == 'seniorBook') {
            if (retainer == 'monthly') {
                +((total = QTY * seniorRateBook).toFixed(2))
                monthTot = (QTY / 12).toFixed(2);
            } else {
                +((total = QTY * seniorRateBook).toFixed(2))
            }

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Senior bookkeeper</p>";
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }else{
                ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }

        } else if (seniorConsultLvl == 'seniorConsult') {
            if (retainer == 'monthly') {
                +((total = QTY * seniorRateConsult).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = QTY * seniorRateConsult).toFixed(2))
            }

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Senior Consultant</p>";
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }else{
                ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }

        } else if (masterLvl == 'master') {
            if (retainer == 'monthly') {
                +((total = QTY * masterRate).toFixed(2))
                monthTot = (total / 12).toFixed(2);
            } else {
                +((total = QTY * masterRate).toFixed(2))
            }
            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Master</p>"
            if (retainer == 'monthly') {
                ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
                ans = ans + "<p>The total amount R" + total + "</p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }else{
                ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                    "<br>" +
                    "</div>" +
                    "<hr>";
            }

        } else {
            +((total = QTY * juniorRateBook).toFixed(2))

            ans =
                "<div id='" + ID + "H'> " +
                "<h4>" + className + "</h4>" +
                "<p>The level of expertise requested: Junior</p>";

            ans = ans + "<p class='Totalprice'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                "<br>" +
                "</div>" +
                "<hr>";
        }

        $("#sideMenu").show();
        $("#selectShow").show();
        $("." + catagory + "").show();

        var IDs = ID + "-id";
        var IDm = ID + "-idM";
        var IDo = ID + "-idO";

        display(retainer, IDs, IDm, IDo, cat, ans);
        Total(cat, total, monthTot, retainer);
    }

    function fixedFunc(ID, fixed, Qty, retainer, catagory, className, cat) {
        var monthTot = 0;
        var ans;
        var total = 0;
        if (retainer == 'monthly') {
            +((total = fixed * Qty).toFixed(2));
            monthTot = (total / 12).toFixed(2);
        } else {
            +((total = fixed * Qty).toFixed(2));
        }

        ans =
            "<div id='" + ID + "Fi'> " +
            "<h4>" + className + "</h4>" +
            "<p>Fixed price: R" + fixed + "</p>" +
            "<p>The Quantity: " + Qty + "</p>";
        if (retainer == 'monthly') {
            ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";
            ans = ans + "<p>The total amount R "+ total + "</p>" +
                "<br>" +
                "</div>" +
                "<hr>";
        }else{
            ans = ans + "<p class='" + catagory + "price'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                "<br>" +
                "</div>" +
                "<hr>";
        }


        $("#sideMenu").show();
        $("#selectShow").show();
        $("." + catagory + "").show();

        var IDs = ID + "-id";
        var IDm = ID + "-idM";
        var IDo = ID + "-idO";

        display(retainer, IDs, IDm, IDo, cat, ans);
        Total(cat, total, monthTot, retainer);
    }

    function pageFunc(ID, fixed, Qty, retainer, catagory, className, cat) {
        var monthTot = 0;
        var ans;
        var total = 0;
        if (retainer == 'monthly') {
            +((total = fixed * Qty).toFixed(2));
            monthTot = (total / 12).toFixed(2);
        } else {
            +((total = fixed * Qty).toFixed(2));
        }

        ans =
            "<div id='" + ID + "Fi'> " +
            "<h4>" + className + "</h4>" +
            "<p>Price per page: R" + fixed + "</p>" +
            "<p>The Quantity: " + Qty + "</p>";
        if (retainer == 'monthly') {
            ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";

            ans = ans + "<p>The total amount R" + total + "</p>" +
                "<br>" +
                "</div>" +
                "<hr>";
        }else{
            ans = ans + "<p class='" + catagory + "price'>The total amount R  <span patts = '" + catagory + "Prices' class='Prices'>" + total + "</span></p>" +
                "<br>" +
                "</div>" +
                "<hr>";
        }


        $("#sideMenu").show();
        $("#selectShow").show();
        $("." + catagory + "").show();

        var IDs = ID + "-id";
        var IDm = ID + "-idM";
        var IDo = ID + "-idO";


        display(retainer, IDs, IDm, IDo, cat, ans);
        Total(cat, total, monthTot, retainer);
    }

    function kmFunc(ID, km, retainer, kmPrice, catagory, className, cat) {

        var price = 0;
        var monthTot = 0;
        if (retainer == 'monthly') {
            +((price = km * kmPrice).toFixed(2));
            monthTot = (price / 12).toFixed(2);
        } else {
            +((price = km * kmPrice).toFixed(2));
        }


        var ans = "<div id='" + ID + "F'> " +
            "<h4>" + className + "</h4>" +
            "<p>The Distance in KM : " + km + "</p>" +
            "<p>Price :</p>";

        if (retainer == 'monthly') {
            ans = ans + "<p class='Monthprice'>The amount due every month R <span patts = '" + catagory + "PricesM' class='" + catagory + "'>" + monthTot + "</span></p>";

            ans = ans +
                "<p> R" + price + "</p>" +
                "<br>" +
                "</div>" +
                "<hr>";

        }else{
            ans = ans +
                "<p class='TotalPrice'> R<span patts = '" + catagory + "Prices' class='Prices'>" + price + "</span></p>" +
                "<br>" +
                "</div>" +
                "<hr>";
        }

        $("#sideMenu").show();
        $("#selectShow").show();
        $("." + catagory + "").show();

        var IDs = ID + "-id";
        var IDm = ID + "-idM";
        var IDo = ID + "-idO";

        display(retainer, IDs, IDm, IDo, cat, ans);
        Total(cat, price, monthTot, retainer);
    }

    function display(retainer, IDs, IDm, IDo, cat, ans) {
        if (retainer == 'monthly') {


            $("." + IDs + " ").append(ans);
            $("." + IDm + " ").append(ans);

            $("." + cat + "-ResultM").show();
            $("#monthlyResults").show();
        } else {

            $("." + IDs + " ").append(ans);
            $("." + IDo + " ").append(ans);

            $("." + cat + "-ResultO").show();
            $("#onceOffResults").show();

        }
    }

    function Total(catagory, total, totalMonth, retainer) {

            var MonthTotal = 0;
            var Totals = 0;

            $('span[patts=' + catagory + '-ResultPricesM]').each(function (index) {
                MonthTotal += parseFloat($(this).text());
            });

        MonthTotal = MonthTotal / 2;

            var adminFee = document.getElementById(catagory + '-FEE').value;

        if (adminFee != 0) {
            adminFee = adminFee / 100;
        }

                if (MonthTotal != 0) {
                    MonthTotal = (MonthTotal * adminFee) + MonthTotal;
                }



            document.getElementById("" + catagory + "-TotalMonth").innerHTML = MonthTotal;
            document.getElementById("" + catagory + "-TotalM-M").innerHTML = MonthTotal;

            var yearMonthly = MonthTotal * 12;

            document.getElementById("" + catagory + "-TotalM").innerHTML = yearMonthly;

            $('span[patts=' + catagory + '-ResultPrices]').each(function (index) {
                Totals += parseFloat($(this).text());
            });


                if (Totals != 0) {
                    Totals = (Totals * adminFee) + Totals;
                }


            Totals = Totals / 2;

            document.getElementById(catagory + "-TotalO").innerHTML = Totals;
            document.getElementById(catagory + "-Total").innerHTML = Totals;

        }

    function totPDFOnce(vatIncluded) {

        var totalOnce = 0;

        $('[attr="TotalO"]').each(function (index) {
            totalOnce = totalOnce + parseFloat($(this).text());
        });

        var temping = totalOnce.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");
        if (vatIncluded) {

            document.getElementById("YearlyY-Total").innerHTML = temping.toString();
            document.getElementById('footerY-vatIncl').style.display = 'block';
            document.getElementById('footerM-vatIncl').style.display = 'block';

            var temp = totalOnce * vatAmount / 100;

            var vat = temp.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");

            document.getElementById("YearlyY-VAT").innerHTML = vat;

            var temp2 = parseFloat(totalOnce) + parseFloat(temp);

            var vatIncl = temp2.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");

            document.getElementById("YearlyY-VATI").innerHTML = vatIncl.toString();

        } else {

            document.getElementById("YearlyY-Total-vatExcl").innerHTML = temping.toString();
            document.getElementById('footerY-vatExcl').style.display = 'block';
            document.getElementById('footerM-vatExcl').style.display = 'block';

        }


    }

    function totPDFMonth(vatIncluded) {

        var totalMonth = 0;

        totalMonth = parseFloat(totalMonth);

        $('[attr="TotalM"]').each(function (index) {
            totalMonth = parseFloat(totalMonth) + parseFloat($(this).text());
        });

        var change = totalMonth.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");

        var temp3;

        if (vatIncluded) {

            document.getElementById("YearlyM-Total").innerHTML = change;
            document.getElementById('footerY-vatIncl').style.display = 'block';
            document.getElementById('footerM-vatIncl').style.display = 'block';

            var vatUnformatted = totalMonth * vatAmount / 100;

            var vatFormatted = vatUnformatted.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");

            document.getElementById("YearlyM-VAT").innerHTML = vatFormatted;

            var temp2 = parseFloat(totalMonth) + parseFloat(vatFormatted);

            temp3 = temp2.toFixed(2);

            var vatIncl = temp2.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");

            document.getElementById("YearlyM-VATI").innerHTML = vatIncl;
        } else {

            document.getElementById('footerY-vatExcl').style.display = 'block';
            document.getElementById('footerM-vatExcl').style.display = 'block';

            temp3 = totalMonth;
        }

        var temp4 = temp3 / 12;

        var monthlyTotal = temp4.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");

        document.getElementById("YearlyM-Total-vatExcl").innerHTML = change;

        document.getElementById("monthly-Total-vatExcl").innerHTML = monthlyTotal;
        document.getElementById("monthly-Total").innerHTML = monthlyTotal;
    }

</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-101081013-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>