<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favIcon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favIcon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favIcon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favIcon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favIcon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favIcon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favIcon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favIcon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favIcon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/favIcon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favIcon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favIcon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favIcon/favicon-16x16.png">
    <meta charset="UTF-8">
    <meta name="keywords"
          content="accounting,Rekenmeester, Rekenmeester Bloemfontein,Belasting,Belasting Bloemfontein,Belasting Voorbereiding,Belasting seisoen,accountant,Tax Preparation Service,TAX season,Bloemfontein accounting,Business Consulting,Human Resource Management,Bloemfontein accountant,Company and Trust Services,Estate Planning,Bloemfontein TAX,Accounting Services,Bookkeeping,Tax Practitioners,Tax Planning">
    <title>Sebenza Group: About Us</title>
    <script src="js/jquery-1.12.3.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id"
          content="1048117067346-iji60dggbnurtrmnm508aqjaqjr7dvia.apps.googleusercontent.com">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->

    <link rel="stylesheet" type="text/css" href="css/main-css/main.css">
</head>

<body data-spy="scroll" data-target="#navbar-side">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="container" style="width: 100%">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <img class="img-responsive" id="logoHead" src="images/sebenzaNew.png"
                             style="">
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav top-nav">
                <li class="" id="home"><a
                        href="/index.php">Home
                </a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class=""><a
                        href="services.php">Services</a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="active" id="aboutUs"><a
                        href="/aboutUs.php">About us</a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="" id="ourStaff">
                    <a
                            href="/ourStaff.php">Our Staff</a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="" id="app">
                    <a
                            href="/app.php">Mobile App</a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="" id="blog">
                    <a
                            href="informationCentre.php">Information centre</a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="" id="contactUs">
                    <a
                            href="contactUS.php">Contact us</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><img class="img-responsive logo" style="max-width: 150px;
    padding-top: 5px;
    margin-bottom: 5px" src="images/sebenzaNew.png"></li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="sebenzaAboutBackground" data-sticky_parent="">
    <div class="container" style="width: 100%; padding-left: 0px; padding-right: 0px">
        <div class="aboutUsSpaceFill"></div>
        <div class="container" style="width: 100%">
            <div class="row" style="height: 5px; background-color: #24464a"></div>
        </div>
        <br>
        <div class="contentFilling">
            <div class="container">
                <div class="row">

                    <div class="col-md-5 col-xs-5 aboutUsFill" style="text-align: center">
                        <h3 style="text-shadow: -1px 0 #24464a, 0 1px #24464a, 1px 0 #24464a, 0 -1px #24464a;color: whitesmoke;">
                            Mission</h3>
                        <hr style="border-top: 1px solid #81772f">
                        <p>
                            To achieve our aims by employing our time, skills and knowledge and utilising the technology
                            and methodology at our disposal to ensure that we deliver on our promise. We firmly believe
                            that our unique approach to our staff and extensive reach in terms of our social
                            responsibility will assist our immediate circle in achieving our ambitious vision.
                        </p>
                    </div>
                    <div class="col-md-2 col-xs-5"></div>
                    <div class="col-md-5 col-xs-5 aboutUsFill" style="text-align: center">
                        <h3 style="text-shadow: -1px 0 #24464a, 0 1px #24464a, 1px 0 #24464a, 0 -1px #24464a;color: whitesmoke;">
                            Vision</h3>
                        <hr style="border-top: 1px solid #81772f">
                        <p>
                            Our aim is to assist entrepreneurs and business owners in the Small Medium Enterprise (SME)
                            in the

                            management and growth of their business. We strive to be industry leaders and ambassadors of
                            our

                            craft and offer a professional service to all of our clients.
                            <br>
                            <br>
                        </p>
                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-md-12 col-xs-12 aboutUsFill">
                        <h3 style="text-shadow: -1px 0 #24464a, 0 1px #24464a, 1px 0 #24464a, 0 -1px #24464a;color: whitesmoke;text-align: center">
                            Our Story</h3>
                        <hr style="border-top: 1px solid #81772f">
                        <p style="text-align: center">Sebenza, meaning “work” or in broader terms “working together”, echoes all that we stand for.
                            Ubuntu is in our blood and our unique approach has been crafted over 30 years of dedication.
                            <br>
                            <br>
                            The company was established in 1987 and has existed with its core staff in one form or
                            another uninterrupted. The founding fathers believed that the industry approach to the Small
                            Medium Enterprise (SME) sectors were diluted and unfocussed and aimed, from day one, to
                            change this approach and improve the perspective of clients in these sectors. Through
                            persistence and an immense will to always go the extra mile they soon established Sebenza as
                            a force to be reckoned with.
                            <br>
                            <br>
                            Effective and Efficient service with a large focus on client satisfaction has kept this
                            vision alive over the past 30 years. We firmly believe that our staff is our most precious
                            asset and by taking care of them they will in turn take good care of our clients, a
                            sentiment first established by the very influential and successful international
                            entrepreneur, Sir Richard Branson.
                            <br>
                            <br>
                            2017 is a very important year for Sebenza as it symbolises the start of the next chapter for
                            our practice. Our confidence in our brand and what it represents has reaped many rewards
                            including an ever-growing and loyal client base necessitating a major expansion. We believe
                            by partnering with the right people and keeping our immaculate service record alive we will
                            be able to even assist more clients in the farther reaches of Southern Africa.
                            Our story has been written with great care over the past 30 years and we hope and believe
                            with great faith that the best chapters are still to come in the next 30 years.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <br>

    </div>
</div>
<script>

    function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;

        post(profile.getId(), profile.getEmail(), profile.getName(), id_token);
    }

    var request;

    function post(ID, email, name, tokens) {

        request = $.ajax({
            type: "POST",
            url: 'phpFiles/login.php',
            data: {ID: ID, userEmail: email, Name: name, token: tokens}
        });

        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            if (response == true) {
                $('#Res').html(response);
            } else {
                $('#Res').html(response);
            }
        });

    }

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-101081013-1', 'auto');
    ga('send', 'pageview');

</script>
<div class="container" style="width: 100%;">
    <div id="footer" class="container" style="width: 100%; padding-right: 0px; padding-left: 0px">
        <div style="background-color:rgba(66, 66, 66, 1); border-top: 3px solid black; padding: 20px; text-align: left;"
             class="footer row">
            <div>
                <div class="col-md-3 col-xs-6">
                    <span style="color: whitesmoke; font-size: 20px;">Company</span>
                    <hr style="border-top: 3px solid #81772f">

                    <a style="color: lightgray; font-size: 18px" href="aboutUs.php">About Us</a>
                    <br>
                    <a style="color: lightgray; font-size: 18px" href="ourStaff.php">Our Staff</a>
                    <br>
                    <a style="color: lightgray; font-size: 18px" href="app.php">Mobile App</a>
                    <br>
                    <br>
                    <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
                    <div ID="Res"></div>
                </div>
                <div class="col-md-3 col-xs-6">
                    <span style="color: whitesmoke; font-size: 20px;">Services</span>
                    <hr style="border-top: 3px solid #81772f">
                    <a style="color: lightgray; font-size: 18px" href="services.php">Accounting
                        Services</a>
                    <br>
                    <a style="color: lightgray; font-size: 18px" href="services.php">Bookkeeping</a>
                    <br>
                    <a style="color: lightgray; font-size: 18px" href="services.php">Tax
                        Practitioners</a>
                    <br>
                    <a style="color: lightgray; font-size: 18px" href="services.php">Tax Planning</a>
                    <br>
                    <a style="color: lightgray; font-size: 18px" href="services.php">Estate
                        Planning</a>
                    <br>
                    <a style="color: lightgray; font-size: 18px" href="services.php">Company and Trust
                        Services</a>
                    <br>
                    <a style="color: lightgray; font-size: 18px" href="services.php">Business
                        Consulting</a>
                    <br>
                    <a style="color: lightgray; font-size: 18px" href="services.php">Human Resource
                        Management</a>

                </div>
                <div class="col-md-3 col-xs-6">

                    <span style="color: whitesmoke; font-size: 20px">Information Centre</span>

                    <hr style="border-top: 3px solid #81772f">
                    <i class="fa fa-facebook-square" style="color: #82713b" aria-hidden="true"></i>
                    <a style="color: lightgray; font-size: 18px"
                       href="https://www.facebook.com/sebenzaaccounting/">Facebook</a>
                    <br>
                    <i class="fa fa-twitter-square" style="color: #82713b" aria-hidden="true"></i>
                    <a style="color: lightgray; font-size: 18px"
                       href="https://twitter.com/SebenzaAcc?lang=en">Twitter</a>
                    <br>
                    <i class="fa fa-users" style="color: #82713b" aria-hidden="true"></i>
                    <a style="color: lightgray; font-size: 18px" href="informationCentre.php">Blog</a>

                </div>
                <div class="col-md-3 col-xs-6">
                    <span style="color: whitesmoke; font-size: 20px">Contact Us</span>
                    <hr style="border-top: 3px solid #81772f">
                    <span style="color: lightgray; font-size: 18px">Phone number: </span><span
                            style="color: #82713b;font-size: 18px">0514301217</span>
                    <br>
                    <span style="text-align: left;color: lightgray; font-size: 18px">Email: </span> <a
                            href="contactUS.php"
                            style="color: #82713b;font-size: 18px"> info@sebenzagroup.co.za</a>
                </div>
            </div>
        </div>
        <div class="row" style="background-color:rgba(66, 66, 66, 1); ">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-xs-5"></div>
                    <div class="col-md-2 col-xs-2" style="text-align: center">
                        <p style="color: whitesmoke">Developed by:</p>
                    </div>
                    <div class="col-md-5 col-xs-5"></div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-xs-5"></div>
                    <div class="col-md-2 col-xs-2"
                         style="background-color:rgba(255, 255, 255, 1); border-radius: 5px">
                        <img class="img-responsive" style="" src="images/partners/sleekLogo.png">
                    </div>
                    <div class="col-md-5 col-xs-5"></div>
                </div>
            </div>
        </div>
        <div class="bottom-text">
            <div class="row" style="background-color:rgba(66, 66, 66,1);">
                <p style="text-align: center; color: whitesmoke; ">Copyright © 2017 Sebenza Group</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>