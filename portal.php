<?php

session_start();

$Name = $_SESSION["Name"];

//if (!isset($_SESSION["Name"])) {
//    header('Location: ' . "index.php");
//}

?>

<HTML>
<HEAD>
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favIcon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favIcon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favIcon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favIcon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favIcon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favIcon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favIcon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favIcon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favIcon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/favIcon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favIcon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favIcon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favIcon/favicon-16x16.png">
    <link rel="manifest" href="/images/favIcon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta charset="UTF-8">
    <meta name="description"
          content="We specialise in accounting, bookkeeping, tax and related services from A to Z including Financial Statement preparation and Tax preparation from consulting to filing with SARS and all tax administration act issues with specific reference to dispute resolution.">
    <meta name="keywords"
          content="accounting,Rekenmeester, Rekenmeester Bloemfontein,Belasting,Belasting Bloemfontein,Belasting Voorbereiding,Belasting seisoen,accountant,Tax Preparation Service,TAX season,Bloemfontein accounting,Business Consulting,Human Resource Management,Bloemfontein accountant,Company and Trust Services,Estate Planning,Bloemfontein TAX,Accounting Services,Bookkeeping,Tax Practitioners,Tax Planning">
    <meta property="og:image" content="http://www.sebenzagroup.co.za/images/sebenzaGost.png"/>
    <link rel=”image_src” href=”http://www.sebenzagroup.co.za/images/sebenzaGost.png”/>
    <title>Portal</title>
    <script src="js/jquery-1.12.3.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id"
          content="1048117067346-iji60dggbnurtrmnm508aqjaqjr7dvia.apps.googleusercontent.com">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.css">

    <!-- Latest compiled and minified JavaScript -->

    <link rel="stylesheet" type="text/css" href="css/main-css/main.css">
</HEAD>
<BODY style=" background: rgba(69, 150, 151, 0.7);">

<div class="container">
    <div class="row">
        <a class="btn btn-default Portal" href="index.php"><span style="font-size: 20px; color: whitesmoke" class="glyphicon glyphicon-home"></span></a>
        <h2 style="text-align: center">Welcome

        </h2>
        <hr>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-3"></div>
        <a target="_blank" href="calculators.php" class="hoverTime" >
            <div class="col-md-2 col-xs-2 Portal-btn" data-toggle="tooltip" data-placement="left" title="Calculator">
                <div class="fa fa-calculator glyph" style=""></div>
            </div>
        </a>
        <div class="col-md-1 col-xs-1"></div>
        <?php
        if (isset($_SESSION["role"])){
            echo "<a target=\"_blank\" href=\"admin.php\" class=\"hoverTime\" >
            <div class=\"col-md-2 col-xs-2 Portal-btn\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Admin\">
                <div class=\"fa fa-user-circle-o glyph\"></div>
            </div>
        </a>";
        }

        ?>

        <div class="col-md-1 col-xs-1"></div>
        <a target="_blank" href="http://sebenzacc.spiceware.net/" class="hoverTime" >
            <div class="col-md-2 col-xs-2 Portal-btn" data-toggle="tooltip" data-placement="right" title="File-O-Mint">
                <div class="fa fa-server glyph"></div>
            </div>
        </a>
        <div class="col-md-3 col-xs-3"></div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <img class="img-responsive" src="images/sebenzaGost.png" style="">
        </div>
        <div class="col-md-4"></div>

    </div>
</div>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
</BODY>
</HTML>
