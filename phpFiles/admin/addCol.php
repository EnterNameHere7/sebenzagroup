<?php

require ("databaseConnect.php");

$sql = "SELECT columnName FROM columnList";

$colName = $_POST["colName"];

$result = $conn->query($sql);

$match = false;

//if there are rows then execute the code below
if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        if ($row["columnName"] == $colName) {
            $match = true;
        }
    }
}

if ($match == false) {
    $sql = "INSERT INTO columnList VALUES ('$colName', 0)";

    if ($conn->query($sql) === TRUE) {
        echo true;
    } else {
        echo false;
    }
} else {
    echo "<div class=\"alert alert-danger\" id=\"userSuccess\" role=\"alert\">Column already exists
            </div>";
}
$conn->close();