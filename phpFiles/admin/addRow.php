<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017/02/23
 * Time: 1:19 PM
 */

require("databaseConnect.php");

echo "HERE";

if (isset($_POST["internRate"])) {
    $interRate = $_POST["internRate"];
} else {
    $interRate = 0;
}

if (isset($_POST["juniorBookRate"])) {
    $juniorBookRate = $_POST["juniorBookRate"];
} else {
    $juniorBookRate = 0;
}

if (isset($_POST["juniorConsultRate"])) {
    $juniorConsultRate = $_POST["juniorConsultRate"];
} else {
   $juniorConsultRate = 0;
}

if (isset($_POST["seniorBookRate"])) {
    $seniorBookRate = $_POST["seniorBookRate"];
} else {
    $seniorBookRate = 0;
}

if (isset($_POST["seniorConsultRate"])) {
    $seniorConsultRate = $_POST["seniorConsultRate"];
} else {
    $seniorConsultRate = 0;
}

if (isset($_POST["masterRate"])) {
    $masterRate = $_POST["masterRate"];
} else {
    $masterRate = 0;
}

if (!empty($_POST["rowName"])) {

    //get info from Post
    $rowName = $_POST["rowName"];

    if (!empty($_POST["payment"])) {
        $once = "0";
        $retain = "0";

        foreach ($_POST["payment"] as $pay){
            if ($pay == "once"){
                $once  = "1";
            }else if ($pay == "retainer"){
                $retain = "1";
            }
        }

        $colName = $_POST["colName"];
        $payment = $_POST["payment"];

        if (!empty($_POST["inputType"])) {

            $inputType = $_POST["inputType"];


            $rowCode = $_POST["rowCode"];

            $sql = "SELECT * FROM rowList WHERE rowCode = '$rowCode'";

            $result = $conn->query($sql);

            if ($result->num_rows > 0){

                echo "<div class=\"alert alert-danger\" id=\"userSuccess\" role=\"alert\">Service code already in use
            </div>";

            }else {

                $sql = "SELECT * FROM rowList WHERE columnName = '$colName'";

                $result = $conn->query($sql);

                $testRes = false;

                if ($result->num_rows > 0) {
                    // output data of each row
                    while ($row = $result->fetch_assoc()) {
                        if ($rowName == $row["rowName"]) {
                            $testRes = true;
                        } else {
                            $testRes = false;
                        }
                    }
                }

                if ($testRes == false) {

//see if value is empty
                    if (!empty($_POST["kmPrice"])) {
                        $priceKM = $_POST["kmPrice"];
                    } else {
                        $priceKM = false;
                    }

                    if (!empty($_POST["pagePrice"])) {
                        $pagePrice = $_POST["pagePrice"];
                    } else {
                        $pagePrice = false;
                    }

//see if value is empty
                    if (!empty($_POST["fixedInput"])) {
                        $fixed = $_POST["fixedInput"];
                    } else {
                        $fixed = false;
                    }
//initialise variables
                    $internExp = false;
                    $juniorConsultExp = false;
                    $juniorBookExp = false;
                    $seniorConsultExp = false;
                    $seniorBookExp = false;
                    $masterExp = false;

//see if value matches any one of the values in the returned array
                    if (!empty($_POST["experience"])) {
                        foreach ($_POST["experience"] as $experience) {

                            if ($experience == "intern") {
                                $internExp = true;
                            }

                            if ($experience == "juniorBook") {
                                $juniorBookExp = true;
                            }

                            if ($experience == "juniorConsult") {
                                $juniorConsultExp = true;
                            }

                            if ($experience == "seniorBook") {
                                $seniorBookExp = true;
                            }

                            if ($experience == "seniorConsult") {
                                $seniorConsultExp = true;
                            }

                            if ($experience == "master") {
                                $masterExp = true;
                            }

                        }
                    } else {
                        $experience = false;
                    }

                    $sql = "INSERT INTO rowList VALUES ('$colName','$rowName','$rowCode','$once' ,'$inputType' ,'$internExp' ,'$juniorBookExp','$juniorConsultExp','$seniorBookExp','$seniorConsultExp' ,'$masterExp' ,'$interRate' ,'$juniorBookRate','$juniorConsultRate' ,'$seniorBookRate','$seniorConsultRate' ,'$masterRate','$priceKM','$fixed','$retain','$pagePrice')";

                    if ($conn->query($sql)) {

                        $sql = "SELECT rowNum FROM columnList WHERE columnName='$colName'";

                        $result = $conn->query($sql);

                        $num = "";

                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {
                                $num = $row["rowNum"];
                            }
                        }

                        $num++;

                        $sql = "UPDATE columnList SET rowNum='$num' WHERE columnName='$colName'";

                        if ($conn->query($sql)) {
                            echo true;
                        } else {
                            echo false;
                        }

                    } else {
                        echo false;
                    }
                } else {
                    echo "<div class=\"alert alert-danger\" id=\"userSuccess\" role=\"alert\">It already exists
            </div>";
                }
            }
        } else {
            echo "<div class=\"alert alert-danger\" id=\"userSuccess\" role=\"alert\">Please select a input type
            </div>";
        }
    } else {
        echo "<div class=\"alert alert-danger\" id=\"userSuccess\" role=\"alert\">Please select a payment method
            </div>";
    }
} else {
    echo "<div class=\"alert alert-danger\" id=\"userSuccess\" role=\"alert\">Please enter a name
            </div>";
}

$conn->close();
