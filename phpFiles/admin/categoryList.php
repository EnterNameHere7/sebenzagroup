<?php


//function to display all columns and services
function genColumns()
{
    require("databaseConnect.php");

    $i = 1;

    $sql = "SELECT * FROM columnList ORDER BY columnName";

    $result = $conn->query($sql);

    //if there are rows then execute the code below
    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $colName = $row["columnName"];
            $rowNumber = $row["rowNum"];

            //first part of the accordion build
            $part1 = "
                    <form class='form-add-group' method='POST'>
                        <div class=\"panel panel-default\">                           
                                <div class=\"panel-heading\">
                                    <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse$i\">$colName</a>
                               
                               <a class='adminSortBtn btn-default pull-right glyphicon glyphicon-sort-by-alphabet-alt' onclick=\"sortColumn('$colName', 'dec') \"></a>
                                    <a class='adminSortBtn btn-default pull-right glyphicon glyphicon-sort-by-alphabet' onclick=\"sortColumn('$colName', 'asc')\"></a>
                                             <button style='border: transparent' data-toggle=\"tooltip\" data-placement=\"right\" title=\"Add rows below\" onclick=\"$('#mainDetails').show(); $('#columnName').attr('value', '$colName')\" class=\"plusRow glyphicon glyphicon-plus pull-right\"></button>
                                                                   <span class=\"badge pull-right\">$rowNumber</span>
                                                                                                                                      
                                    </h4>
                                </div>
                                <div id=\"collapse$i\" class=\"panel-collapse collapse\">
                                    <ul id='$colName' class=\"list-group\">
                                    ";

            $part2 = "";

            $sql = "SELECT rowName,rowCode FROM rowList WHERE columnName = '$colName' ORDER BY columnOrderNum ASC";

            $results = $conn->query($sql);

            //build the services list
            if ($results->num_rows > 0) {
                // output data of each row
                $h = 0;
                while ($row = $results->fetch_assoc()) {
                    $val = $row["rowName"];
                    $rowCode = $row["rowCode"];

                    $val2 = urlencode($val);
                    $temp = "
                                <li class='list-group-item $colName-group' row-code='$rowCode' draggable='true'>
                                   <a href='#'>
                                        $val  
                                        <a class='pull-right glyphicon glyphicon-trash minBtn'  draggable='true' href='#' onclick='deleteRowFromDB(\"$val2\",\"$colName\")'></a>
                                   </a>
                                </li>";
                    $h++;
                    $part2 = $part2 . $temp;
                }
            }


            //final part of the accordion
            $part3 = " </ul>
 
 <script>
                         $(document).ready(function () {
                            $('#$colName').sortable({
                                axis: 'y',
                                update: function (event, ui) {
                                    
                                    var articleorder= '$colName';
                                     $('#$colName li').each(function(i) {
                                         
                                      articleorder += ',' + $(this).attr('row-code');
                                     
                                      });
                                     
                                    $.post('phpFiles/admin/updateRowOrder.php', { order: articleorder })
                                    .error(function(data) { 
                                     alert('Error: ' + data); 
                                     }); 
                                }
                            });
                        });
</script>
 
                                </div>
                            </div>
                            <input style='display: none' value='$colName' name='colName'>
                              </form>";
            $i++;

            //display the accordion
            echo $part1 . $part2 . $part3;
        }
    } else {
        //display this if there are no columns
        echo "<div class=\"panel panel-default\">
                                <div class=\"panel-heading\">
                                    <h4 class=\"panel-title\">
                                        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1\">
                                            This is how category will be displayed<a class=\"glyphicon glyphicon-plus pull-right\"
                                                                   href=\"#\" data-target=\"#errorModal\" data-toggle=\"modal\" data-placement=\"right\" title=\"Add rows below\"></a>
                                                                   <span class=\"badge pull-right\">1</span>
                                                                   </a>
                                    </h4>
                                </div>
                                <div id=\"collapse1\" class=\"panel-collapse collapse in\">
                                    <ul class=\"list-group\">
                                        <li class=\"list-group-item\">
                                            <a id='fakeTrash' href=\"#\">
                                                This is how services will be shown<button style='border: transparent;background-color: white'  onclick=\"$('#fakeTrash').hide();\"  class=\"pull-right glyphicon glyphicon-trash minBtn\"
                                                                   href=\"#\"></button>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>";
    }

    $conn->close();
}
