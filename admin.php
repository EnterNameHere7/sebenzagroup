<?php
//all the required files
require("phpFiles/admin/categoryList.php");
?>

<HEAD>
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favIcon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favIcon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favIcon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favIcon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favIcon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favIcon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favIcon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favIcon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favIcon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/favIcon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favIcon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favIcon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favIcon/favicon-16x16.png">
    <link rel="manifest" href="/images/favIcon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta charset="UTF-8">
    <meta name="description"
          content="We specialise in accounting, bookkeeping, tax and related services from A to Z including Financial Statement preparation and Tax preparation from consulting to filing with SARS and all tax administration act issues with specific reference to dispute resolution.">
    <meta name="keywords"
          content="accounting,Rekenmeester, Rekenmeester Bloemfontein,Belasting,Belasting Bloemfontein,Belasting Voorbereiding,Belasting seisoen,accountant,Tax Preparation Service,TAX season,Bloemfontein accounting,Business Consulting,Human Resource Management,Bloemfontein accountant,Company and Trust Services,Estate Planning,Bloemfontein TAX,Accounting Services,Bookkeeping,Tax Practitioners,Tax Planning">
    <meta property="og:image" content="http://www.sebenzagroup.co.za/images/sebenzaGost.png"/>
    <link rel=”image_src” href=”http://www.sebenzagroup.co.za/images/sebenzaGost.png”/>
    <title>Admin</title>
    <script src="js/jquery-1.12.3.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id"
          content="1048117067346-iji60dggbnurtrmnm508aqjaqjr7dvia.apps.googleusercontent.com">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">

    <!-- Latest compiled and minified JavaScript -->

    <link rel="stylesheet" type="text/css" href="css/main-css/main.css">
</HEAD>
<body style=" background: rgba(69, 150, 151, 0.7);">
<div id="mainBackground" class="container" style="width: 100%">
    <div class="container" style="width: 100%">
        <div class="row">
            <nav style="width: 100%" class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php">Sebenza</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="admin.php">Admin<span class="sr-only">(current)</span></a></li>
                            <li><a href="calculators.php">Calculator</a></li>
                            <li><a href="portal.php">File-O-Mint</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div class="container" style="width: 100%">
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="float">
                    <div class="btn btn-default" onclick="location.reload()"><span
                                class="glyphicon glyphicon-repeat"></span> Refresh
                    </div>
                    <button type="button" class="btn btn-default" data-toggle="modal"
                            data-target="#ratesModal"><span class="glyphicon glyphicon-usd"></span> Default rates
                    </button>
                    <br>
                    <br>
                    <div class="genBack">
                        <div class="panel-group" id="accordion">
                            <?php
                            genColumns()
                            ?>
                            <script>
                                function sortColumn(colName,order) {


                                    $.post('phpFiles/admin/updateRowOrderAlphabetically.php', {
                                        colName: colName,
                                        order: order
                                    })
                                        .success(function (data) {
                                            document.getElementById(colName).innerHTML = data;
                                        })
                                        .error(function (data) {
                                            alert('Error: ' + data);
                                        });
                                }
                            </script>
                        </div>
                    </div>
                    <br>
                    <button class="btn btn-default" type="button" data-toggle="modal" data-target="#myModal">Add
                        category
                    </button>
                    <button class="btn btn-default pull-right" type="button" data-toggle="modal"
                            data-target="#delModal">Remove
                    </button>
                </div>
            </div>
            <script>

                //version
                console.log("VERSION 0.2.1");
            </script>
            <div class="col-md-1"></div>
            <div class="col-md-6">
                <div id="show-Details" style="padding-left: 10px; padding-right: 10px"></div>
                <div id="mainDetails" class="float">
                    <h3 style="text-align: center; text-decoration: underline">Details</h3>
                    <form method="post" id="details">
                        <div class="row">

                                <h4>Service name</h4>
                                <div class="container" style="width: auto">
                                    <div class="row">
                                        <div class="col-md-8 col-xs-8">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1">Service</span>
                                                <input type="text" name='rowName' class="form-control"
                                                       placeholder="service"
                                                       aria-describedby="basic-addon1">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1">Service code</span>
                                                <input type="text" name='rowCode' class="form-control"
                                                       placeholder="service code"
                                                       aria-describedby="basic-addon1">
                                            </div>
                                        </div>

                                        <div class="input-group">
                                            <input style="display: none" type="text" id="columnName" name='colName'
                                                   class="form-control"
                                                   aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <hr>
                        <h4>Payment method</h4>
                        <div class="container" style="width: auto;">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="input-group">
                                        <div class="checkbox">
                                            <label><input name="payment[]" id="once" type="checkbox" value="once"> -
                                                Once
                                                off</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input name="payment[]" id="include" type="checkbox"
                                                          value="retainer">
                                                -
                                                Include
                                                in
                                                retainer</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5"></div>
                            </div>
                        </div>
                        <hr>
                        <div id="show-Me" style="display: none">
                            <h4>Input type</h4>
                            <div class="container" style="width: auto">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <label style="font-weight:normal"><input id="kmTravel"
                                                                                     style="visibility: visible;position: relative;"
                                                                                     name="inputType"
                                                                                     type="radio" value="KM"> -
                                                KM</label>
                                            <br>
                                            <label style="font-weight:normal"><input id="hours"
                                                                                     style="visibility: visible;position: relative;"
                                                                                     name="inputType"
                                                                                     type="radio"
                                                                                     value="Hours"> -
                                                Hours</label>
                                            <br>
                                            <label style="font-weight:normal"><input id="fixed"
                                                                                     style="visibility: visible;position: relative;"
                                                                                     name="inputType"
                                                                                     type="radio"
                                                                                     value="Fixed"> - Fixed
                                                price</label>
                                            <br>
                                            <label style="font-weight:normal"><input id="Dynfixed"
                                                                                     style="visibility: visible;position: relative;"
                                                                                     name="inputType"
                                                                                     type="radio"
                                                                                     value="Dynfixed"> -Dynamic fixed
                                                price</label>
                                            <br>
                                            <label style="font-weight:normal"><input id="pages"
                                                                                     style="visibility: visible;position: relative;"
                                                                                     name="inputType"
                                                                                     type="radio"
                                                                                     value="Pages"> - Pages</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5"></div>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div id="hourShowMe" style='display: none'>

                            <h4>Experience level</h4>
                            <div class="container" style="width: auto">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="intern" onclick="
                                                                      if($('#internRate').prop('disabled')){
                                                                          $('#internRate').prop('disabled', false);
                                                                      }else{
                                                                          $('#internRate').prop('disabled', true);
                                                                          $('#internRate').val('')
                                                                      }"> - Intern</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <label for="usr">Default hours:</label>
                                            <input type="text" id="internRate"
                                                   class="form-control"
                                                   name="internRate">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="juniorBook" onclick="
                                                                      if($('#juniorBookRate').prop('disabled')){
                                                                          $('#juniorBookRate').prop('disabled', false);
                                                                      }else{
                                                                          $('#juniorBookRate').prop('disabled', true);
                                                                          $('#juniorBookRate').val('')
                                                                      }"> - Junior bookkeeper</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <label for="usr">Default hours:</label>
                                            <input type="text" class="form-control"
                                                   id="juniorBookRate" name="juniorBookRate">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="juniorConsult" onclick="
                                                                      if($('#juniorConsultRate').prop('disabled')){
                                                                          $('#juniorConsultRate').prop('disabled', false);
                                                                      }else{
                                                                          $('#juniorConsultRate').prop('disabled', true);
                                                                          $('#juniorConsultRate').val('')
                                                                      }"> - Junior consultant</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <label for="usr">Default hours:</label>
                                            <input type="text" class="form-control"
                                                   id="juniorConsultRate" name="juniorConsultRate">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="seniorBook" onclick="
                                                                      if($('#seniorBookRate').prop('disabled')){
                                                                          $('#seniorBookRate').prop('disabled', false);
                                                                      }else{
                                                                          $('#seniorBookRate').prop('disabled', true);
                                                                          $('#seniorBookRate').val('')
                                                                      }"> - Senior Bookkeeper</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <label for="usr">Default hours:</label>
                                            <input type="text" class="form-control"
                                                   name="seniorBookRate" id="seniorBookRate">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="seniorConsult" onclick="
                                                                      if($('#seniorConsultRate').prop('disabled')){
                                                                          $('#seniorConsultRate').prop('disabled', false);
                                                                      }else{
                                                                          $('#seniorConsultRate').prop('disabled', true);
                                                                          $('#seniorConsultRate').val('')
                                                                      }"> - Senior consultant</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <label for="usr">Default hours:</label>
                                            <input type="text" class="form-control"
                                                   name="seniorConsultRate" id="seniorConsultRate">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="master" onclick="
                                                                      if($('#masterRate').prop('disabled')){
                                                                          $('#masterRate').prop('disabled', false);
                                                                      }else{
                                                                          $('#masterRate').prop('disabled', true);
                                                                          $('#masterRate').val('')
                                                                      }"> - Master</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <label for="usr">Default hours:</label>
                                            <input type="text" class="form-control"
                                                   name="masterRate" id="masterRate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div id="kmShowMe" style='display: none'>
                            <h4>Price per KM</h4>
                            <div class="container" style="width: auto">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                                    <span class="input-group-addon"
                                                          id="basic-addon1">Price per km R:</span>
                                            <input id="kmTravelInput" name="kmPrice" type="text"
                                                   class="form-control"
                                                   placeholder=""
                                                   aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div id="fixed-Show-Me" style="display: none">
                            <h4>Fixed Price</h4>
                            <div class="container" style="width: auto">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                                    <span class="input-group-addon"
                                                          id="basic-addon1">Fixed price R:</span>
                                            <input name="fixedInput" type="text" class="form-control"
                                                   placeholder=""
                                                   aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div id="dynFixed-ShowMe" style='display: none'>

                            <h4>Experience level</h4>
                            <div class="container" style="width: auto">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="intern"> - Intern</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="juniorBook" onclick=""> - Junior bookkeeper</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="juniorConsult" onclick=""> - Junior consultant</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="seniorBook" onclick=""> - Senior Bookkeeper</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="seniorConsult" onclick=""> - Senior consultant</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-xs-6">
                                            <div class="checkbox">
                                                <label><input style="position: relative; margin-left: 0px"
                                                              name="experience[]" type="checkbox"
                                                              value="master" onclick=""> - Master</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div id="pages-Show-Me" style="display: none">
                            <h4>Page Price</h4>
                            <div class="container" style="width: auto">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                                    <span class="input-group-addon"
                                                          id="basic-addon1">Price per page R:</span>
                                            <input name="pagePrice" type="text" class="form-control"
                                                   placeholder=""
                                                   aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                            <hr>
                        </div>

                        <button type='submit' class="btn btn-default">Submit</button>
                        <button type='reset' class="btn btn-default pull-right"
                                onclick="$('#mainDetails').hide();">
                            Cancel
                        </button>
                </div>
                <div class="col-md-1"></div>
            </div>
            </form>
        </div>
        <script>

            $("#internRate").prop('disabled', true);
            $("#juniorBookRate").prop('disabled', true);
            $("#juniorConsultRate").prop('disabled', true);
            $("#seniorBookRate").prop('disabled', true);
            $("#seniorConsultRate").prop('disabled', true);
            $('#masterRate').prop('disabled', true);

            $('#kmTravel').change(function () {
                $('#kmShowMe').show();
                $('#kmTravelInput').prop('required', true);
                $('#hourShowMe').hide();
                $('#pages-Show-Me').hide();
                $('#fixed-Show-Me').hide();
                $('#dynFixed-ShowMe').hide();
            });

            $('#hours').change(function () {
                $('#hourShowMe').show();
                $('#kmShowMe').hide();
                $('#pages-Show-Me').hide();
                $('#fixed-Show-Me').hide();
                $('#dynFixed-ShowMe').hide();
                $('#kmTravelInput').prop('required', false);
            });

            $('#once').change(function () {
                $('#fixed-Show-Me').hide();
                $('#show-Me').show();
            });

            $('#include').change(function () {
                $('#fixed-Show-Me').hide();
                $('#show-Me').show();
            });

            $('#fixed').change(function () {
                $('#pages-Show-Me').hide();
                $('#fixed-Show-Me').show();
                $('#hourShowMe').hide();
                $('#kmShowMe').hide();
                $('#dynFixed-ShowMe').hide();
            });

            $('#Dynfixed').change(function () {
                $('#pages-Show-Me').hide();
                $('#fixed-Show-Me').hide();
                $('#dynFixed-ShowMe').show();
                $('#hourShowMe').hide();
                $('#kmShowMe').hide();
            });

            $('#pages').change(function () {
                $('#pages-Show-Me').show();
                $('#fixed-Show-Me').hide();
                $('#hourShowMe').hide();
                $('#kmShowMe').hide();
                $('#dynFixed-ShowMe').hide();
            });

        </script>
    </div>
</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="addCol" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Adding categories</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">category name</span>
                                <input type="text" class="form-control" name="colName"
                                       placeholder="example: Bookkeeping"
                                       aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-5"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add category</button>
                </div>
                <div id="result"></div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="delCol" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Removing categories</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Category name</span>
                                <input type="text" class="form-control" name="colName"
                                       placeholder="example: Bookkeeping"
                                       aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-5"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Remove category</button>
                </div>
                <div id="delresult"></div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" role="alert"> Please first add a category</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ratesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="rates" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <?php
                        require("phpFiles/admin/databaseConnect.php");

                        $SQL = "SELECT * FROM Rates";

                        $result = $conn->query($SQL);

                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {

                                $inter = $row["internRate"];
                                $juniorBook = $row["juniorRateBook"];
                                $seniorBook = $row["seniorRateBook"];
                                $juniorConsult = $row["juniorRateConsult"];
                                $seniorConsult = $row["seniorRateConsult"];
                                $master = $row["masterRate"];

                                echo "<label for=\"usr\">Intern rate:</label>
                            <input type=\"text\" required name='internRate' class=\"form-control\" value='$inter' id=\"usr\">";

                                echo "<label for=\"usr\">Junior bookkeeper rate:</label>
                            <input type=\"text\" required name='juniorRateBook' class=\"form-control\" value='$juniorBook' id=\"usr\">";

                                echo "<label for=\"usr\">Junior consultant rate:</label>
                            <input type=\"text\" required name='juniorRateConsult' class=\"form-control\" value='$juniorConsult' id=\"usr\">";

                                echo "<label for=\"usr\">Senior bookkeeper rate:</label>
                            <input type=\"text\" required name='seniorRateBook' class=\"form-control\" value='$seniorBook' id=\"usr\">";

                                echo "<label for=\"usr\">Senior consultant rate:</label>
                            <input type=\"text\" required name='seniorRateConsult' class=\"form-control\" value='$seniorConsult' id=\"usr\">";

                                echo "<label for=\"usr\">Master rate:</label>
                            <input type=\"text\" required name='masterRate' class=\"form-control\" value='$master' id=\"usr\">";
                            }
                        }
                        ?>
                        <br>
                        <div id="ratesResult"></div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>

<script>

    $("#mainDetails").hide();

    var request;

    $("#addCol").submit(function (event) {

        // Prevent default posting of form - put here to work in case of errors
        event.preventDefault();

        // Abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $(this);

        // Let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");

        // Serialize the data in the form
        var serializedData = $form.serialize();

        // Let's disable the inputs for the duration of the Ajax request.
        // Note: we disable elements AFTER the form data has been serialized.
        // Disabled form elements will not be serialized.
        $inputs.prop("disabled", true);


        request = $.ajax({
            url: "phpFiles/admin/addCol.php",
            type: "post",
            data: serializedData
        });


        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            if (response == true) {
                location.reload()
            } else {
                $('#result').html(response);
            }
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // Log the error to the console
            console.error(
                "The following error occurred: " +
                textStatus, errorThrown
            );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // Reenable the inputs
            $inputs.prop("disabled", false);
        });

    });

    $("#delCol").submit(function (event) {

        // Prevent default posting of form - put here to work in case of errors
        event.preventDefault();

        // Abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $(this);

        // Let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");

        // Serialize the data in the form
        var serializedData = $form.serialize();

        // Let's disable the inputs for the duration of the Ajax request.
        // Note: we disable elements AFTER the form data has been serialized.
        // Disabled form elements will not be serialized.
        $inputs.prop("disabled", true);


        request = $.ajax({
            url: "phpFiles/admin/delCol.php",
            type: "post",
            data: serializedData
        });


        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            if (response == true) {
                location.reload()
            } else {
                $('#delresult').html(response);
            }
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // Log the error to the console
            console.error(
                "The following error occurred: " +
                textStatus, errorThrown
            );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // Reenable the inputs
            $inputs.prop("disabled", false);
        });

    });

    $("#details").submit(function (event) {

        // Prevent default posting of form - put here to work in case of errors
        event.preventDefault();

        // Abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $(this);

        // Let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");

        // Serialize the data in the form
        var serializedData = $form.serialize();

        // Let's disable the inputs for the duration of the Ajax request.
        // Note: we disable elements AFTER the form data has been serialized.
        // Disabled form elements will not be serialized.
        $inputs.prop("disabled", true);


        request = $.ajax({
            url: "phpFiles/admin/addRow.php",
            type: "post",
            data: serializedData
        });


        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            if (response == true) {
                location.reload()
            } else {
                $('#show-Details').html(response);
            }
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // Log the error to the console
            console.error(
                "The following error occurred: " +
                textStatus, errorThrown
            );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // Reenable the inputs
            $inputs.prop("disabled", false);
        });

        $(':input').not(':button, :submit, :reset, :hidden').removeAttr('checked').removeAttr('selected').not('‌​:checkbox, :radio, select').val('');

        resets();

    });

    function  resets() {
        $("#details")[0].reset();
    }

    function deleteRowFromDB(row,category){
        // Prevent default posting of form - put here to work in case of errors
        event.preventDefault();

        // Abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $(this);

        // Let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");

        // Serialize the data in the form
        var serializedData = $form.serialize();

        // Let's disable the inputs for the duration of the Ajax request.
        // Note: we disable elements AFTER the form data has been serialized.
        // Disabled form elements will not be serialized.
        $inputs.prop("disabled", true);

        console.log(row + " " + category)

        request = $.ajax({
            url: "delRow.php",
            type: "post",
            data: {'name':row,'colName':category}
        });


        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            if (response == true) {
                location.reload()
            } else {
                $('#show-Details').html(response);
            }
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // Log the error to the console
            console.error(
                "The following error occurred: " +
                textStatus, errorThrown
            );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // Reenable the inputs
            $inputs.prop("disabled", false);
        });

        $(':input').not(':button, :submit, :reset, :hidden').removeAttr('checked').removeAttr('selected').not('‌​:checkbox, :radio, select').val('');

        resets();

    }

    $(".form-add-group").submit(function (event) {

        // Prevent default posting of form - put here to work in case of errors
        event.preventDefault();

        // Abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $(this);

        // Let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");

        // Serialize the data in the form
        var serializedData = $form.serialize();

        // Let's disable the inputs for the duration of the Ajax request.
        // Note: we disable elements AFTER the form data has been serialized.
        // Disabled form elements will not be serialized.
        $inputs.prop("disabled", true);


        request = $.ajax({
            url: "phpFiles/admin/genDetails.php",
            type: "post",
            data: serializedData
        });


        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            $('#show-Details').html(response);
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // Log the error to the console
            console.error(
                "The following error occurred: " +
                textStatus, errorThrown
            );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // Reenable the inputs
            $inputs.prop("disabled", false);
        });

    });

    $("#rates").submit(function (event) {

        // Prevent default posting of form - put here to work in case of errors
        event.preventDefault();

        // Abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $(this);

        // Let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");

        // Serialize the data in the form
        var serializedData = $form.serialize();

        // Let's disable the inputs for the duration of the Ajax request.
        // Note: we disable elements AFTER the form data has been serialized.
        // Disabled form elements will not be serialized.
        $inputs.prop("disabled", true);


        request = $.ajax({
            url: "phpFiles/admin/setRate.php",
            type: "post",
            data: serializedData
        });


        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            if (response == true) {
                location.reload()
            } else {
                $('#ratesResult').html(response);
            }
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // Log the error to the console
            console.error(
                "The following error occurred: " +
                textStatus, errorThrown
            );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // Reenable the inputs
            $inputs.prop("disabled", false);
        });

    });
</script>
</body>
