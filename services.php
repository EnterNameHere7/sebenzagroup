<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favIcon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favIcon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favIcon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favIcon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favIcon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favIcon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favIcon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favIcon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favIcon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/favIcon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favIcon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favIcon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favIcon/favicon-16x16.png">
    <meta charset="UTF-8">
    <meta name="description" content="Services provided by Sebenza group">
    <meta name="keywords"
          content="accounting,Rekenmeester, Rekenmeester Bloemfontein,Belasting,Belasting Bloemfontein,Belasting Voorbereiding,Belasting seisoen,accountant,Tax Preparation Service,TAX season,Bloemfontein accounting,Business Consulting,Human Resource Management,Bloemfontein accountant,Company and Trust Services,Estate Planning,Bloemfontein TAX,Accounting Services,Bookkeeping,Tax Practitioners,Tax Planning">
    <title>Sebenza Group: Services</title>
    <script src="js/jquery-1.12.3.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id"
          content="1048117067346-iji60dggbnurtrmnm508aqjaqjr7dvia.apps.googleusercontent.com">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->

    <link rel="stylesheet" type="text/css" href="css/main-css/main.css">
</head>
<body class="sebenzaBackground">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="container" style="width: 100%">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <img class="img-responsive" id="logoHead" src="images/sebenzaNew.png"
                             style="">
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav top-nav">
                <li class="" id="home"><a
                        href="/index.php">Home
                </a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="active"><a
                        href="services.php">Services</a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="" id="aboutUs"><a
                        href="/aboutUs.php">About us</a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="" id="ourStaff">
                    <a
                            href="/ourStaff.php">Our Staff</a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="" id="app">
                    <a
                            href="/app.php">Mobile App</a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="" id="blog">
                    <a
                            href="informationCentre.php">Information centre</a>
                </li>
                <li><span class="btn-separator"></span></li>
                <li class="" id="contactUs">
                    <a
                            href="contactUS.php">Contact us</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><img class="img-responsive logo" style="max-width: 150px;
    padding-top: 5px;
    margin-bottom: 5px" src="images/sebenzaNew.png"></li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container" style="width: 100%; padding-right: 0px; padding-left: 0px">
    <div class="serviceSpaceFill">
    </div>
    <div class="container" style="width: 100%">
        <div class="row" style="background-color: white;">
            <div class="PageDevider">
                <h2 style="color: grey; text-align: center; border-bottom: 3px solid #81772f; padding-bottom: 10px">How
                    we help
                </h2>
            </div>
        </div>
    </div>
    <div class="container" style="width: 100%">
        <div class="row" style="margin: 25px">
            <div style="padding-top: 60px;"></div>
            <div class="col-md-3 col-xs-6">
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <div class="front"
                             style="box-shadow: 2px 5px 5px grey;background-color:#2F3738; border-radius: 6px; ">
                            <img class="img-responsive"
                                 src="/images/sebenzaStockFotos/financialStatements.jpeg"
                                 alt=""
                            >
                            <h2 class="overlay" style="border-radius: 3px">Accounting Services</h2>
                        </div>
                        <div class="back">
                            <div style="height: 100%; min-width: 100%; border-radius: 6px; ">
                                <p class="overlay2" style="font-size: 12px">
                                    If you are a practicing professional, farmer, entrepreneur or business owner in
                                    this sector you are exactly the type of client that will fit perfectly into our
                                    approach. We can assist you with the following Accounting Services:
                                    <br>
                                    <span class="glyphicon glyphicon-arrow-right"></span>Annual Financial
                                    Statemets<br>
                                    <span class="glyphicon glyphicon-arrow-right"></span>Interim Financial
                                    Statements<br>
                                    <span class="glyphicon glyphicon-arrow-right"></span>Monthly/Quarterly/Annual
                                    Management Statements<br>
                                    <span class="glyphicon glyphicon-arrow-right"></span>Independent Reviews

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <div class="front"
                             style="box-shadow: 2px 5px 5px grey; border-radius: 6px; ">
                            <img class="img-responsive" src="/images/sebenzaStockFotos/bookkeeping.jpg" alt=""
                                 style="">
                            <h2 class="overlay" style="border-radius: 3px">Bookkeeping</h2>
                        </div>
                        <div class="back">
                            <div style="height: 100%; min-width: 100%; border-radius: 6px; ">
                                <p class="overlay2" style="font-size: 12px">
                                    At Sebenza we believe that technology saves time and time saves money.
                                    Accordingly we make use of the latest technology in our bookkeeping approach
                                    enabling you as the client to be informed and in control whilst saving you time
                                    and money.
                                    All of these software packages are integrated with this App to provide you with
                                    fingertip access on demand.</p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <div class="front"
                             style="box-shadow: 2px 5px 5px grey;background-color:#2F3738; border-radius: 6px; ">
                            <img class="img-responsive"
                                 src="/images/sebenzaStockFotos/sarsEfiling.jpeg"
                                 alt=""
                            >
                            <h2 class="overlay" style="border-radius: 3px">Tax Practitioners</h2>
                        </div>
                        <div class="back">
                            <div style="height: 100%; min-width: 100%; border-radius: 6px; ">

                                <p class="overlay2" style="font-size: 12px">
                                    We have a team of dedicated Tax Practitioners specialising in Individuals tax that
                                    will be able to assist you with the following:
                                    <br>
                                    <span class="glyphicon glyphicon-arrow-right"></span>Income Tax submission<br>
                                    <span class="glyphicon glyphicon-arrow-right"></span>E-Filing profile setup and
                                    management<br>
                                    <span class="glyphicon glyphicon-arrow-right"></span>Tax Reviews<br>
                                    <span class="glyphicon glyphicon-arrow-right"></span>Tax Audits<br>
                                    <span class="glyphicon glyphicon-arrow-right"></span>SARS profile management<br>
                                    <span class="glyphicon glyphicon-arrow-right"></span>Tax advice and planning<br>
                                    </ol>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <div class="front"
                             style="box-shadow: 2px 5px 5px grey; border-radius: 6px; ">
                            <img class="img-responsive"
                                 src="/images/sebenzaStockFotos/taxPlanning.jpg" alt=""
                                 style="">
                            <h2 class="overlay" style="border-radius: 3px">Tax Planning</h2>
                        </div>
                        <div class="back">
                            <div style="height: 100%; min-width: 100%; border-radius: 6px; ">
                                <p class="overlay2" style="font-size: 15px">
                                    With our 30 years of experience in this industry and our team of experts we can
                                    assist you in all your tax related queries ranging from SARS compliance to future
                                    tax planning and advice surrounding Individuals, Companies and Trusts.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <div class="row" style="margin: 25px">
            <div class="col-md-3 col-xs-6">
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <div class="front"
                             style="box-shadow: 2px 5px 5px grey;background-color:#2F3738; border-radius: 6px; ">
                            <img class="img-responsive" src="/images/sebenzaStockFotos/estatePlanning.jpg"
                                 alt=""
                            >
                            <h2 class="overlay" style="border-radius: 3px">Estate Planning</h2>
                        </div>
                        <div class="back">
                            <div class="hovereffect">
                                <div style="height: 100%; min-width: 100%; border-radius: 6px; ">
                                    <p class="overlay2" style="font-size: 12px">
                                        We are able to provide you with the following services and advice on this
                                        subject:
                                        <br>
                                        <span class="glyphicon glyphicon-arrow-right"></span>Risk analysis based on
                                        financial information<br>
                                        <span class="glyphicon glyphicon-arrow-right"></span>Life-Insurance
                                        consultations and suggestions based on risk analysis<br>
                                        <span class="glyphicon glyphicon-arrow-right"></span>Drafting of Last Will
                                        and Testament<br>
                                        <span class="glyphicon glyphicon-arrow-right"></span>Administration of
                                        Estates<br>
                                        <span class="glyphicon glyphicon-arrow-right"></span>Execution of Estates in
                                        conjunction with the Master of the High Court
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="flip-container">
                    <div class="flipper">
                        <div class="front"
                             style="box-shadow: 2px 5px 5px grey; border-radius: 6px; ">
                            <img class="img-responsive" src="/images/sebenzaStockFotos/independentReviews.jpg" alt=""
                                 style="">
                            <h2 class="overlay" style="border-radius: 3px">Company and Trust Services</h2>
                        </div>
                        <div class="back">
                            <div style="height: 100%; min-width: 100%; border-radius: 6px; ">
                                <p class="overlay2" style="font-size: 13px">
                                    In conjunction with the TBS Group, more
                                    specifically the
                                    Independent Consultant branch, we help our clients register new Companies with CIPC
                                    and new
                                    Trusts with the Master of the High Court. Our after registration service also
                                    includes
                                    independent Trustee services, Company Secretary services and all support services
                                    like
                                    resolutions and independent reporting.</p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <div class="front"
                             style="box-shadow: 2px 5px 5px grey;background-color:#2F3738; border-radius: 6px; ">
                            <img class="img-responsive"
                                 src="/images/sebenzaStockFotos/structurePlanning.jpeg" alt=""
                            >
                            <h2 class="overlay" style="border-radius: 3px">Business Consulting</h2>
                        </div>
                        <div class="back">
                            <div class="hovereffect">
                                <div style="height: 100%; min-width: 100%; border-radius: 6px; ">
                                    <p class="overlay2" style="font-size: 12px;">We add value through the following:
                                        <br>
                                        <span class="glyphicon glyphicon-arrow-right"></span>Business acquisitions and
                                        selling<br>
                                        <span class="glyphicon glyphicon-arrow-right"></span>Business valuations<br>
                                        <span class="glyphicon glyphicon-arrow-right"></span>BEE advice and ratings<br>
                                        <span class="glyphicon glyphicon-arrow-right"></span>Application for funding
                                        through institutions like the NEF<br>
                                        <span class="glyphicon glyphicon-arrow-right"></span>Project management out of a
                                        accounting perspective

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <div class="front"
                             style="box-shadow: 2px 5px 5px grey;background-color:#2F3738; border-radius: 6px; ">
                            <img class="img-responsive"
                                 src="/images/sebenzaStockFotos/managementStatements.jpeg"
                                 alt=""
                            >
                            <h2 class="overlay" style="border-radius: 3px">Human Resource Management</h2>
                        </div>
                        <div class="back">
                            <div style="height: 100%; min-width: 100px; border-radius: 6px; ">
                                <p class="overlay2" style="font-size: 12px">
                                    We have dedicated staff members working day in and day out in close proximity with
                                    the
                                    Department of Labour to assist our clients in the management of one of their most
                                    precious
                                    assets. Whether you need new registrations, amendments or help with the compensation
                                    commissioner we are here to assist. We also include the Unemployment Insurance Fund
                                    (UIF)
                                    services as standard with all our retainer clients.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
    </div>
    <div class="container" style="width: 100%;">
        <div id="footer" class="container" style="width: 100%; padding-right: 0px; padding-left: 0px">
            <div style="background-color:rgba(66, 66, 66, 1); border-top: 3px solid black; padding: 20px; text-align: left;"
                 class="footer row">
                <div>
                    <div class="col-md-3 col-xs-6">
                        <span style="color: whitesmoke; font-size: 20px;">Company</span>
                        <hr style="border-top: 3px solid #81772f">

                        <a style="color: lightgray; font-size: 18px" href="aboutUs.php">About Us</a>
                        <br>
                        <a style="color: lightgray; font-size: 18px" href="ourStaff.php">Our Staff</a>
                        <br>
                        <a style="color: lightgray; font-size: 18px" href="app.php">Mobile App</a>
                        <br>
                        <br>
                        <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
                        <div ID="Res"></div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <span style="color: whitesmoke; font-size: 20px;">Services</span>
                        <hr style="border-top: 3px solid #81772f">
                        <a style="color: lightgray; font-size: 18px" href="services.php">Accounting
                            Services</a>
                        <br>
                        <a style="color: lightgray; font-size: 18px" href="services.php">Bookkeeping</a>
                        <br>
                        <a style="color: lightgray; font-size: 18px" href="services.php">Tax
                            Practitioners</a>
                        <br>
                        <a style="color: lightgray; font-size: 18px" href="services.php">Tax Planning</a>
                        <br>
                        <a style="color: lightgray; font-size: 18px" href="services.php">Estate
                            Planning</a>
                        <br>
                        <a style="color: lightgray; font-size: 18px" href="services.php">Company and Trust
                            Services</a>
                        <br>
                        <a style="color: lightgray; font-size: 18px" href="services.php">Business
                            Consulting</a>
                        <br>
                        <a style="color: lightgray; font-size: 18px" href="services.php">Human Resource
                            Management</a>

                    </div>
                    <div class="col-md-3 col-xs-6">

                        <span style="color: whitesmoke; font-size: 20px">Information Centre</span>

                        <hr style="border-top: 3px solid #81772f">
                        <i class="fa fa-facebook-square" style="color: #82713b" aria-hidden="true"></i>
                        <a style="color: lightgray; font-size: 18px"
                           href="https://www.facebook.com/sebenzaaccounting/">Facebook</a>
                        <br>
                        <i class="fa fa-twitter-square" style="color: #82713b" aria-hidden="true"></i>
                        <a style="color: lightgray; font-size: 18px"
                           href="https://twitter.com/SebenzaAcc?lang=en">Twitter</a>
                        <br>
                        <i class="fa fa-users" style="color: #82713b" aria-hidden="true"></i>
                        <a style="color: lightgray; font-size: 18px" href="informationCentre.php">Blog</a>

                    </div>
                    <div class="col-md-3 col-xs-6">
                        <span style="color: whitesmoke; font-size: 20px">Contact Us</span>
                        <hr style="border-top: 3px solid #81772f">
                        <span style="color: lightgray; font-size: 18px">Phone number: </span><span
                                style="color: #82713b;font-size: 18px">0514301217</span>
                        <br>
                        <span style="text-align: left;color: lightgray; font-size: 18px">Email: </span> <a
                                href="contactUS.php"
                                style="color: #82713b;font-size: 18px"> info@sebenzagroup.co.za</a>
                    </div>
                </div>
            </div>
            <div class="row" style="background-color:rgba(66, 66, 66, 1); ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-xs-5"></div>
                        <div class="col-md-2 col-xs-2" style="text-align: center">
                            <p style="color: whitesmoke">Developed by:</p>
                        </div>
                        <div class="col-md-5 col-xs-5"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-xs-5"></div>
                        <div class="col-md-2 col-xs-2"
                             style="background-color:rgba(255, 255, 255, 1); border-radius: 5px">
                            <img class="img-responsive" style="" src="images/partners/sleekLogo.png">
                        </div>
                        <div class="col-md-5 col-xs-5"></div>
                    </div>
                </div>
            </div>
            <div class="bottom-text">
                <div class="row" style="background-color:rgba(66, 66, 66,1);">
                    <p style="text-align: center; color: whitesmoke; ">Copyright © 2017 Sebenza Group</p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;

        post(profile.getId(), profile.getEmail(), profile.getName(), id_token);
    }

    var request;

    function post(ID, email, name, tokens) {

        request = $.ajax({
            type: "POST",
            url: 'phpFiles/login.php',
            data: {ID: ID, userEmail: email, Name: name, token: tokens}
        });

        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            if (response == true) {
                $('#Res').html(response);
            } else {
                $('#Res').html(response);
            }
        });

    }

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-101081013-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>